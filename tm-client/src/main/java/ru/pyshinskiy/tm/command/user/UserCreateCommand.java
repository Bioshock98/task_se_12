package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.command.AbstractCommand;

public final class UserCreateCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        return true;
    }

    @Override
    @NotNull
    public String command() {
        return "user_create";
    }

    @Override
    @NotNull
    public String description() {
        return "create new user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CREATE]");
        System.out.println("ENTER USERNAME");
        @NotNull final String login = terminalService.nextLine();
        System.out.println("ENTER PASSWORD");
        @NotNull final String password = terminalService.nextLine();
        @NotNull final Role role = Role.USER;
        bootstrap.getUserEndpoint().persistUser(login, password, role);
        System.out.println("[OK]");
    }
}

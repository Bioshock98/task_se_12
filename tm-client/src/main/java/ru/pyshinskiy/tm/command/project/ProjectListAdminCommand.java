package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.api.endpoint.Session;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectListAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        if(bootstrap.getSession() == null) return false;
        return bootstrap.getSession().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "project_list_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "list all users projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        System.out.println("DO YOU WANT TO SORT PROJECTS?");
        @NotNull final String doSort = bootstrap.getTerminalService().nextLine();
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        @NotNull final Session session = bootstrap.getSession();
        if("y".equals(doSort) || "yes".equals(doSort)) {
            System.out.println("CHOOSE SORT TYPE");
            System.out.println("[createTime, startDate, finishDate, status]");
            @NotNull final String option = bootstrap.getTerminalService().nextLine();
            switch (option) {
                case "createTime" :
                    printProjects(projectEndpoint.sortProjectsByCreateTime(bootstrap.getSession(), session.getUserId(), 1));
                    break;
                case "startDate" :
                    printProjects(projectEndpoint.sortProjectsByStartDate(bootstrap.getSession(), session.getUserId(), 1));
                    break;
                case "finishDate" :
                    printProjects(projectEndpoint.sortProjectsByFinishDate(bootstrap.getSession(), session.getUserId(), 1));
                    break;
                case "status" :
                    printProjects(projectEndpoint.sortProjectsByStatus(bootstrap.getSession(), session.getUserId(), 1));
            }
            System.out.println("[OK]");
        }
        else {
            printProjects(projectEndpoint.findAllProjects(bootstrap.getSession()));
            System.out.println("[OK]");
        }
    }
}

package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Session;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project_list";
    }

    @Override
    @NotNull
    public String description() {
        return "show all projects";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = bootstrap.getProjectEndpoint();
        System.out.println("[PROJECT LIST]");
        System.out.println("CHOOSE SORT TYPE");
        System.out.println("[createTime, startDate, finishDate, status]");
        @NotNull final String option = bootstrap.getTerminalService().nextLine();
        @NotNull final Session session = bootstrap.getSession();
        switch (option) {
            case "createTime" :
                printProjects(projectEndpoint.sortProjectsByCreateTime(bootstrap.getSession(), session.getUserId(), 1));
                break;
            case "startDate" :
                printProjects(projectEndpoint.sortProjectsByStartDate(bootstrap.getSession(), session.getUserId(), 1));
                break;
            case "finishDate" :
                printProjects(projectEndpoint.sortProjectsByFinishDate(bootstrap.getSession(), session.getUserId(), 1));
                break;
            case "status" :
                printProjects(projectEndpoint.sortProjectsByStatus(bootstrap.getSession(), session.getUserId(), 1));
        }
        System.out.println("[OK]");
    }
}

package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.user.IUserRepository;
import ru.pyshinskiy.tm.constant.FieldConst;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumerated.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    private User fetch(@Nullable final ResultSet row) throws Exception {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString(FieldConst.ID));
        user.setLogin(row.getString(FieldConst.LOGIN));
        user.setPassword(row.getString(FieldConst.PASSWORD_HASH));
        user.setRole(Role.valueOf(row.getString(FieldConst.ROLE)));
        return user;
    }

    @Override
    @NotNull
    public List<User> findAll() throws Exception {
        @NotNull final String query = "select * from taskmanager.app_user";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<User> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    @Nullable
    public User findOne(@NotNull final String id) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_user where id=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @NotNull final User user = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return user;
    }

    @Override
    @NotNull
    public User persist(@NotNull final User user) throws Exception {
        @NotNull final String query = "insert into taskmanager.app_user values (?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, user.getId());
        preparedStatement.setString(2, user.getLogin());
        preparedStatement.setString(3, user.getPassword());
        preparedStatement.setString(4, String.valueOf(user.getRole()));
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return user;
    }

    @Override
    @NotNull
    public List<User> persist(@NotNull final List<User> users) throws Exception {
        for(@NotNull final User user : users) {
            @NotNull final String query = "insert into taskmanager.app_user values (?, ?, ?, ?)";
            @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1, user.getId());
            preparedStatement.setString(2, user.getLogin());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setString(4, String.valueOf(user.getRole()));
            preparedStatement.executeUpdate();
            preparedStatement.close();
        }
        return users;
    }

    @Override
    @NotNull
    public User merge(@NotNull final User user) throws Exception {
        @NotNull final String query = "update taskmanager.app_user set login=?, passwordHash=?, role=? where id=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, user.getId());
        preparedStatement.setString(2, user.getPassword());
        preparedStatement.setString(3, String.valueOf(user.getRole()));
        preparedStatement.setString(4, user.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return user;
    }

    @Override
    public void remove(@NotNull final String id) throws Exception {
        @NotNull final String query = "delete from taskmanager.app_user where id=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, id);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String query = "delete from taskmanager.app_user";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    @Nullable
    public User getUserByLogin(@NotNull final String login) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_user where login=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, login);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final User user = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return user;
    }
}

package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.task.ITaskRepository;
import ru.pyshinskiy.tm.constant.FieldConst;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.enumerated.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateToSQLDate;

public final class TaskRepository extends AbstractWBSRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @Nullable
    protected Task fetch(@Nullable final ResultSet row) throws Exception {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString(FieldConst.ID));
        task.setUserId(row.getString(FieldConst.USER_ID));
        task.setCreateDate(row.getDate(FieldConst.CREATE_TIME));
        task.setName(row.getString(FieldConst.NAME));
        task.setDescription(row.getString(FieldConst.DESCRIPTION));
        task.setStatus(Status.valueOf(row.getString(FieldConst.STATUS)));
        task.setStartDate(row.getDate(FieldConst.START_DATE));
        task.setFinishDate(row.getDate(FieldConst.FINISH_DATE));
        return task;
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_task where userId =?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;

    }

    @Override
    @NotNull
    public List<Task> findAll() throws Exception {
        @NotNull final String query = "select * from taskmanager.app_task";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;

    }

    @Override
    @Nullable
    public Task findOne(@NotNull final String id) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_task where id=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final Task task = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return task;
    }

    @Override
    @Nullable
    public Task findOne(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_task where userId=? and id=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final Task task = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return task;
    }

    @Override
    @Nullable
    public Task persist(@NotNull final Task task) throws Exception {
        @NotNull final String query = "insert into taskmanager.app_task values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, task.getId());
        preparedStatement.setString(2, task.getUserId());
        preparedStatement.setString(3, task.getProjectId());
        preparedStatement.setDate(4, parseDateToSQLDate(task.getCreateDate()));
        preparedStatement.setString(5, task.getName());
        preparedStatement.setString(6, task.getDescription());
        preparedStatement.setString(7, String.valueOf(task.getStatus()));
        preparedStatement.setDate(8, parseDateToSQLDate(task.getStartDate()));
        preparedStatement.setDate(9, parseDateToSQLDate(task.getFinishDate()));
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return task;
    }

    @Override
    @NotNull
    public List<Task> persist(@NotNull final List<Task> tasks) throws Exception {
        for(@NotNull final Task task : tasks) {
            @NotNull final String query = "insert into taskmanager.app_task values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1, task.getId());
            preparedStatement.setString(2, task.getUserId());
            preparedStatement.setString(3, task.getProjectId());
            preparedStatement.setDate(4, parseDateToSQLDate(task.getCreateDate()));
            preparedStatement.setString(5, task.getName());
            preparedStatement.setString(6, task.getDescription());
            preparedStatement.setString(7, String.valueOf(task.getStatus()));
            preparedStatement.setDate(8, parseDateToSQLDate(task.getStartDate()));
            preparedStatement.setDate(9, parseDateToSQLDate(task.getFinishDate()));
            preparedStatement.close();
        }
        return tasks;
    }

    @Override
    @NotNull
    public Task merge(@NotNull final Task task) throws Exception {
        @NotNull final String query = "update taskmanager.app_task set userId=?, projectId=?, createTime=?, name=?, description=?, status=?, startDate=?, finishDate=? where id=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, task.getUserId());
        preparedStatement.setString(2, task.getProjectId());
        preparedStatement.setDate(3, parseDateToSQLDate(task.getCreateDate()));
        preparedStatement.setString(4, task.getName());
        preparedStatement.setString(5, task.getDescription());
        preparedStatement.setString(6, String.valueOf(task.getStatus()));
        preparedStatement.setDate(7, parseDateToSQLDate(task.getStartDate()));
        preparedStatement.setDate(8, parseDateToSQLDate(task.getFinishDate()));
        preparedStatement.setString(9, task.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return task;
    }

    @Override
    public void remove(@NotNull final String id) throws Exception {
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement("delete from taskmanager.app_task where id=?");
        preparedStatement.setString(1, id);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement("delete from taskmanager.app_task where userId=? and id=?");
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement("delete from taskmanager.app_task");
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void removeAll(@NotNull final String userId) throws Exception {
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement("delete from taskmanager.app_task where userId=?");
        preparedStatement.setString(1, userId);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @NotNull
    public List<Task> findByName(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_task where userId = ? and name = ?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, name);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    @NotNull
    public List<Task> findByDescription(@NotNull final String userId, @NotNull final String description) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_task where userId = ? and description = ?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, description);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    @NotNull
    public List<Task> sortByCreateTime(@NotNull final String userId, final int direction) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_task where userId=? order by createTime";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    @NotNull
    public List<Task> sortByStartDate(@NotNull final String userId, final int direction) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_task where userId=? order by startDate";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    @NotNull
    public  List<Task> sortByFinishDate(@NotNull final String userId, final int direction) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_task where userId=? order by finishDate";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    @NotNull
    public List<Task> sortByStatus(@NotNull final String userId, final int direction) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_task where userId=? order by FIELD(status, 'PLANNED', 'IN_PROGRESS', 'DONE')";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }
    @Override
    @NotNull
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_task where userId=? and projectId=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement("delete from taskmanager.app_task where userId=?");
        preparedStatement.setString(1, userId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void removeAttachedTasks() throws Exception {
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement("delete from taskmanager.app_task where projectId is not null");
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @Override
    public void removeAttachedTasks(@NotNull final String userId) throws Exception {
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement("delete from taskmanager.app_task where userId=? and projectId is not null");
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }
}

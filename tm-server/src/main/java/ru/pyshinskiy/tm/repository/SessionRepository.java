package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.session.ISessionRepository;
import ru.pyshinskiy.tm.constant.FieldConst;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.enumerated.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    private Session fetch(@Nullable final ResultSet row) throws Exception {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString(FieldConst.ID));
        session.setUserId(row.getString(FieldConst.USER_ID));
        session.setRole(Role.valueOf(row.getString(FieldConst.ROLE)));
        session.setTimestamp(row.getTimestamp(FieldConst.TIMESTAMP).getTime());
        session.setSignature(row.getString(FieldConst.SIGNATURE));
        return session;
    }

    @Override
    @NotNull
    public List<Session> findAll() throws Exception {
        @NotNull final String query = "select * from taskmanager.app_session";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Session> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    @Nullable
    public Session findOne(@NotNull final String id) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_session where id=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final Session session = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return session;
    }

    @Override
    public Session persist(@NotNull final Session session) throws Exception {
        @NotNull final String query = "insert into taskmanager.app_session value(?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, session.getId());
        preparedStatement.setString(2, session.getUserId());
        preparedStatement.setString(3, String.valueOf(session.getRole()));
        preparedStatement.setTimestamp(4, new Timestamp(session.getTimestamp()));
        preparedStatement.setString(5, session.getSignature());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return session;
    }

    @Override
    @NotNull
    public List<Session> persist(@NotNull final List<Session> sessions) throws Exception {
        for(@NotNull final Session session : sessions) {
            @NotNull final String query = "insert into taskmanager.app_session value(?, ?, ?, ?, ?)";
            @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1, session.getId());
            preparedStatement.setTimestamp(2, new Timestamp(session.getTimestamp()));
            preparedStatement.setString(3, session.getUserId());
            preparedStatement.setString(4, String.valueOf(session.getRole()));
            preparedStatement.setString(5, session.getSignature());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        }
        return sessions;
    }

    @Override
    @NotNull
    public Session merge(@NotNull final Session session) throws Exception {
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement("update taskmanager.app_session set userId=?," +
                "timestamp=?, role=?, signature=? where id=?");
        preparedStatement.setString(1, session.getUserId());
        preparedStatement.setTimestamp(2, new Timestamp(session.getTimestamp()));
        preparedStatement.setString(3, String.valueOf(session.getRole()));
        preparedStatement.setString(4, session.getSignature());
        preparedStatement.setString(5, session.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return session;
    }

    @Override
    public void remove(@NotNull final String id) throws Exception {
        @NotNull final String query = "delete from taskmanager.app_session where id=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, id);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String query = "delete from taskmanager.app_session";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @Nullable
    public Session findOne(@NotNull final String userId, @NotNull final String id) throws Exception{
        @NotNull final String query = "select * from taskmanager.app_session where userId=? and id=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final Session session = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return session;
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String query = "delete from taskmanager.app_session where userId=? and id=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        preparedStatement.execute();
        preparedStatement.close();
    }
}

package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.repository.IRepository;
import ru.pyshinskiy.tm.entity.AbstractEntity;

import java.sql.Connection;
import java.util.List;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @NotNull
    private final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    public Connection getConnection() {
        return connection;
    }

    @NotNull
    @Override
    abstract public List<T> findAll() throws Exception;

    @Nullable
    @Override
    abstract public T findOne(@NotNull final String id) throws Exception;

    @Nullable
    @Override
    abstract public T persist(@NotNull final T t) throws Exception;

    @NotNull
    @Override
    abstract public List<T> persist(@NotNull final List<T> ts) throws Exception;

    @Nullable
    @Override
    abstract public T merge(@NotNull final T t) throws Exception;

    @Override
    abstract public void remove(@NotNull final String id) throws Exception;

    @Override
    abstract public void removeAll() throws Exception;
}

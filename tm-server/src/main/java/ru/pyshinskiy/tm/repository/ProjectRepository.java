package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.project.IProjectRepository;
import ru.pyshinskiy.tm.constant.FieldConst;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.enumerated.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateToSQLDate;

public final class ProjectRepository extends AbstractWBSRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @Nullable
    protected Project fetch(@Nullable final ResultSet row) throws Exception {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString(FieldConst.ID));
        project.setUserId(row.getString(FieldConst.USER_ID));
        project.setCreateDate(row.getDate(FieldConst.CREATE_TIME));
        project.setName(row.getString(FieldConst.NAME));
        project.setDescription(row.getString(FieldConst.DESCRIPTION));
        project.setStatus(Status.valueOf(row.getString(FieldConst.STATUS)));
        project.setStartDate(row.getDate(FieldConst.START_DATE));
        project.setFinishDate(row.getDate(FieldConst.FINISH_DATE));
        return project;
    }

    @Override
    @NotNull
    public List<Project> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_project where userId = ?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    @NotNull
    public List<Project> findAll() throws Exception {
        @NotNull final String query = "select * from taskmanager.app_project";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    @Nullable
    public Project findOne(@NotNull final String id) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_project where id=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final Project project = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return project;
    }

    @Override
    @Nullable
    public Project findOne(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_project where userId=? and id=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        @Nullable final Project project = fetch(resultSet);
        resultSet.close();
        preparedStatement.close();
        return project;
    }

    @Override
    @NotNull
    public Project persist(@NotNull final Project project) throws Exception {
        @NotNull final String query = "insert into taskmanager.app_project values (?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, project.getId());
        preparedStatement.setString(2, project.getUserId());
        preparedStatement.setDate(3, new java.sql.Date(project.getCreateDate().getTime()));
        preparedStatement.setString(4, project.getName());
        preparedStatement.setString(5, project.getDescription());
        preparedStatement.setString(6, String.valueOf(project.getStatus()));
        preparedStatement.setDate(7, parseDateToSQLDate(project.getStartDate()));
        preparedStatement.setDate(8, parseDateToSQLDate(project.getFinishDate()));
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return project;
    }

    @Override
    @NotNull
    public List<Project> persist(@NotNull final List<Project> projects) throws Exception {
        for(@NotNull final Project project : projects) {
            @NotNull final String query = "insert into taskmanager.app_project values (?, ?, ?, ?, ?, ?, ?, ?)";
            @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1, project.getId());
            preparedStatement.setString(2, project.getUserId());
            preparedStatement.setDate(3, new java.sql.Date(project.getCreateDate().getTime()));
            preparedStatement.setString(4, project.getName());
            preparedStatement.setString(5, project.getDescription());
            preparedStatement.setString(6, String.valueOf(project.getStatus()));
            preparedStatement.setDate(7, parseDateToSQLDate(project.getStartDate()));
            preparedStatement.setDate(8, parseDateToSQLDate(project.getFinishDate()));
            preparedStatement.executeUpdate();
            preparedStatement.close();
        }
        return projects;
    }

    @Override
    @NotNull
    public Project merge(@NotNull final Project project) throws Exception {
        @NotNull final String query = "update taskmanager.app_project set userId=?," +
                " createDate=?, name=?, description=?, status=?, startDate=?, finishDate=? where id=?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, project.getUserId());
        preparedStatement.setDate(2, new java.sql.Date(project.getCreateDate().getTime()));
        preparedStatement.setString(3, project.getName());
        preparedStatement.setString(4, project.getDescription());
        preparedStatement.setString(5, String.valueOf(project.getStatus()));
        preparedStatement.setDate(6, parseDateToSQLDate(project.getStartDate()));
        preparedStatement.setDate(7, parseDateToSQLDate(project.getStartDate()));
        preparedStatement.setString(8, project.getId());
        preparedStatement.executeUpdate();
        preparedStatement.close();
        return project;
    }

    @Override
    public void remove(@NotNull final String id ) throws Exception {
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement("delete from taskmanager.app_project where id=?");
        preparedStatement.setString(1, id);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement("delete from taskmanager.app_project where userId=? and id=?");
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, id);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement("delete from taskmanager.app_project");
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    public void removeAll(@NotNull final String userId) throws Exception {
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement("delete from taskmanager.app_project where userId=?");
        preparedStatement.setString(1, userId);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    @NotNull
    public List<Project> findByName(@NotNull final String userId, @NotNull final String name) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_project where userId = ? and name = ?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, name);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }


    @Override
    @NotNull
    public List<Project> findByDescription(@NotNull final String userId, @NotNull final String description) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_project where userId = ? and description = ?";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, description);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    @NotNull
    public List<Project> sortByCreateTime(@NotNull final String userId, final int direction) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_project where userId=? order by createTime";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    @NotNull
    public List<Project> sortByStartDate(@NotNull final String userId, final int direction) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_project where userId=? order by startDate";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    @NotNull
    public  List<Project> sortByFinishDate(@NotNull final String userId, final int direction) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_project where userId=? order by finishDate";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }

    @Override
    @NotNull
    public List<Project> sortByStatus(@NotNull final String userId, final int direction) throws Exception {
        @NotNull final String query = "select * from taskmanager.app_project where userId=? order by FIELD(status, 'PLANNED', 'IN_PROGRESS', 'DONE')";
        @NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        resultSet.close();
        preparedStatement.close();
        return result;
    }
}

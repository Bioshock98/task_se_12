package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.wbs.IAbstractWBSRepository;
import ru.pyshinskiy.tm.entity.AbstractWBS;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public abstract class AbstractWBSRepository<T extends AbstractWBS> extends AbstractRepository<T> implements IAbstractWBSRepository<T> {

    protected AbstractWBSRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    abstract protected T fetch(@Nullable final ResultSet row) throws SQLException, Exception;

    @NotNull
    abstract public List<T> findAll(@NotNull final String userId) throws Exception;

    @NotNull
    @Override
    abstract public List<T> findAll() throws Exception;

    @Nullable
    @Override
    abstract public T findOne(@NotNull final String id) throws Exception;

    @Nullable
    abstract public T findOne(@NotNull final String userId, @NotNull final String id) throws Exception;

    @Nullable
    @Override
    abstract public T persist(@NotNull final T t) throws Exception;

    @NotNull
    @Override
    abstract public List<T> persist(@NotNull final List<T> ts) throws Exception;

    @Nullable
    @Override
    abstract public T merge(@NotNull final T t) throws Exception;

    @Override
    abstract public void remove(@NotNull final String id) throws Exception;

    @Override
    abstract public void remove(@NotNull final String userId, @NotNull final String id) throws Exception;

    @Override
    abstract public void removeAll() throws Exception;

    abstract public void removeAll(@NotNull final String userId) throws Exception;

    @Nullable
    @Override
    abstract public List<T> findByName(@NotNull final String userId, @NotNull final String name) throws Exception;

    @Nullable
    @Override
    abstract public List<T> findByDescription(@NotNull final String userId, @NotNull final String description) throws Exception;

    @Override
    @NotNull
    abstract public List<T> sortByCreateTime(@NotNull final String userId, final int direction) throws Exception;

    @Override
    @NotNull
    abstract public List<T> sortByStartDate(@NotNull final String userId, final int direction) throws Exception;

    @Override
    @NotNull
    abstract public List<T> sortByFinishDate(@NotNull final String userId, final int direction) throws Exception;

    @Override
    @NotNull
    abstract public List<T> sortByStatus(@NotNull final String userId, final int direction) throws Exception;
}

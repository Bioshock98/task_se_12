package ru.pyshinskiy.tm;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;

public final class App {

    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }
}

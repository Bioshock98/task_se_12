package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class ProjectService extends AbstractWBSService<Project> implements IProjectService {

    @Nullable
    @Override
    public Project findOne(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("project id is empty or null");
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).findOne(userId, id);
    }

    @Override
    @NotNull
    public List<Project> findAll(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).findAll(userId);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("project id is empty or null");
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new ProjectRepository(connection).remove(userId, id);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new ProjectRepository(connection).removeAll(userId);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    @NotNull
    public List<Project> findByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        if(name == null || name.isEmpty()) throw new Exception("name is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).findByName(userId, name);
    }

    @Override
    @NotNull
    public List<Project> findByDescription(@Nullable final String userId, @Nullable final String description) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        if(description == null || description.isEmpty()) throw new Exception("description is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).findByDescription(userId, description);
    }

    @Override
    @NotNull
    public List<Project> sortByCreateTime(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).sortByCreateTime(userId, direction);
    }

    @Override
    public @NotNull List<Project> sortByStartDate(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).sortByStartDate(userId, direction);
    }

    @Override
    @NotNull
    public List<Project> sortByFinishDate(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).sortByFinishDate(userId, direction);
    }

    @Override
    @NotNull
    public List<Project> sortByStatus(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).sortByStatus(userId, direction);
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("project id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).findOne(id);
    }

    @Override
    @NotNull
    public List<Project> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new ProjectRepository(connection).findAll();
    }

    @NotNull
    @Override
    public Project persist(@Nullable final Project project) throws Exception {
        if(project == null) throw new Exception("invalid project");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new ProjectRepository(connection).persist(project);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return project;
    }

    @Override
    @NotNull
    public List<Project> persist(@NotNull final List<Project> projects) throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new ProjectRepository(connection).persist(projects);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return projects;
    }

    @NotNull
    @Override
    public Project merge(@Nullable final Project project) throws Exception {
        if(project == null) throw new Exception("invalid project");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new ProjectRepository(connection).merge(project);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return project;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if(id == null) throw new Exception("invalid project id");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new ProjectRepository(connection).remove(id);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new ProjectRepository(connection).removeAll();
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }
}

package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.session.ISessionService;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.repository.SessionRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class SessionService extends AbstractService<Session>  implements ISessionService {

    @Override
    @Nullable
    public Session findOne(@Nullable final String userId, @Nullable final String id) throws Exception{
        if(userId == null) throw new Exception("invalid user id");
        if(id == null) throw new Exception("invalid session id");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new SessionRepository(connection).findOne(userId, id);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(userId == null) throw new Exception("invalid user id");
        if(id == null) throw new Exception("invalid session id");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new SessionRepository(connection).remove(userId, id);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String id) throws Exception {
        if(id == null) throw new Exception("invalid session id");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new SessionRepository(connection).findOne(id);
    }

    @Override
    public @NotNull List<Session> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new SessionRepository(connection).findAll();
    }

    @NotNull
    @Override
    public Session persist(@Nullable final Session session) throws Exception {
        if(session == null) throw new Exception("invalid session");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new SessionRepository(connection).persist(session);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return session;
    }

    @Override
    @NotNull
    public List<Session> persist(@NotNull final List<Session> sessions) throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new SessionRepository(connection).persist(sessions);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return sessions;
    }

    @NotNull
    @Override
    public Session merge(@Nullable final Session session) throws Exception {
        if(session == null) throw new Exception("invalid session");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new SessionRepository(connection).merge(session);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return session;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if(id == null) throw new Exception("invalid session id");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new SessionRepository(connection).remove(id);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new SessionRepository(connection).removeAll();
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }
}

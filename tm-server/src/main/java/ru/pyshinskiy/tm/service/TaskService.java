package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class TaskService extends AbstractWBSService<Task> implements ITaskService {

    @Nullable
    @Override
    public Task findOne(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("task id is empty or null");
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).findOne(userId, id);
    }

    @Override
    @NotNull
    public List<Task> findAll(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).findAll(userId);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("task id is empty or null");
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new TaskRepository(connection).remove(userId, id);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new TaskRepository(connection).removeAll(userId);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    @NotNull
    public List<Task> findByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        if(name == null || name.isEmpty()) throw new Exception("name is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).findByName(userId, name);
    }

    @Override
    @NotNull
    public List<Task> findByDescription(@Nullable final String userId, @Nullable final String description) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        if(description == null || description.isEmpty()) throw new Exception("description is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).findByDescription(userId, description);
    }

    @Override
    @NotNull
    public List<Task> sortByCreateTime(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).sortByCreateTime(userId, direction);
    }

    @Override
    public @NotNull List<Task> sortByStartDate(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).sortByStartDate(userId, direction);
    }

    @Override
    @NotNull
    public List<Task> sortByFinishDate(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).sortByFinishDate(userId, direction);
    }

    @Override
    @NotNull
    public List<Task> sortByStatus(@Nullable final String userId, final int direction) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).sortByStatus(userId, direction);
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("task id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).findOne(id);
    }

    @Override
    @NotNull
    public List<Task> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).findAll();
    }

    @NotNull
    @Override
    public Task persist(@Nullable final Task task) throws Exception {
        if(task == null) throw new Exception("invalid task");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new TaskRepository(connection).persist(task);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return task;
    }

    @Override
    @NotNull
    public List<Task> persist(@NotNull final List<Task> tasks) throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new TaskRepository(connection).persist(tasks);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return tasks;
    }

    @NotNull
    @Override
    public Task merge(@Nullable final Task task) throws Exception {
        if(task == null) throw new Exception("invalid task");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new TaskRepository(connection).merge(task);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return task;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if(id == null) throw new Exception("invalid task id");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new TaskRepository(connection).remove(id);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new TaskRepository(connection).removeAll();
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if(userId == null || projectId == null) throw new Exception("one of the parameters passed is zero");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new TaskRepository(connection).findAllByProjectId(userId, projectId);
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if(userId == null) throw new Exception("invalid user id");
        if(projectId == null) throw new Exception("invalid session id");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new TaskRepository(connection).removeAllByProjectId(userId, projectId);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    public void removeAttachedTasks() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        new TaskRepository(connection).removeAttachedTasks();
    }

    @Override
    public void removeAttachedTasks(@Nullable final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        new TaskRepository(connection).removeAttachedTasks(userId);
    }
}

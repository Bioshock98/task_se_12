package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.repository.UserRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    @Nullable
    @Override
    public User findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("user id is empty or null");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new UserRepository(connection).findOne(id);
    }

    @Override
    @NotNull
    public List<User> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new UserRepository(connection).findAll();
    }

    @NotNull
    @Override
    public User persist(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("invalid user");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new UserRepository(connection).persist(user);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return user;
    }

    @Override
    @NotNull
    public List<User> persist(@NotNull final List<User> users) throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new UserRepository(connection).persist(users);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return users;
    }

    @NotNull
    @Override
    public User merge(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("invalid user");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new UserRepository(connection).merge(user);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
        return user;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if(id == null) throw new Exception("invalid user id");
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new UserRepository(connection).remove(id);
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            new UserRepository(connection).removeAll();
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
        }
        finally {
            connection.close();
        }
    }

    @Override
    @Nullable
    public User getUserByLogin(@Nullable final String login) throws Exception {
        if(login == null) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        return new UserRepository(connection).getUserByLogin(login);
    }
}

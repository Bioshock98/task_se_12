package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.service.IService;
import ru.pyshinskiy.tm.entity.AbstractEntity;
import ru.pyshinskiy.tm.util.database.ConnectionUtil;

import java.sql.Connection;
import java.util.List;

public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @NotNull
    public Connection getConnection() throws Exception {
        return ConnectionUtil.getConnection();
    }
    @Nullable
    @Override
    abstract public T findOne(@Nullable final String id) throws Exception;

    @NotNull
    @Override
    abstract public List<T> findAll() throws Exception;

    @Nullable
    @Override
    abstract public T persist(@Nullable final T t) throws Exception;

    @NotNull
    @Override
    abstract public List<T> persist(@NotNull final List<T> ts) throws Exception;

    @Nullable
    @Override
    abstract public T merge(@Nullable final T t) throws Exception;

    @Override
    abstract public void remove(@Nullable final String id) throws Exception;

    @Override
    abstract public void removeAll() throws Exception;
}

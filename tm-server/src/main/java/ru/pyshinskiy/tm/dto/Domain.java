package ru.pyshinskiy.tm.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement
public class Domain implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlElement
    @Nullable
    private List<Project> projects;

    @XmlElement
    @Nullable
    private List<Task> tasks;

    @XmlElement
    @Nullable
    private List<User> users;

    @Nullable
    public List<Project> getProjects() {
        return projects;
    }

    @Nullable
    public List<Task> getTasks() {
        return tasks;
    }

    @Nullable
    public List<User> getUsers() {
        return users;
    }

    public void load(@NotNull final IProjectService projectService, @NotNull final ITaskService taskService,
                     @NotNull final IUserService userService, @NotNull final String userId) throws Exception {

        this.projects = new LinkedList<>(projectService.findAll(userId));
        this.tasks = new LinkedList<>(taskService.findAll(userId));
        this.users = new LinkedList<>(userService.findAll());
    }
}

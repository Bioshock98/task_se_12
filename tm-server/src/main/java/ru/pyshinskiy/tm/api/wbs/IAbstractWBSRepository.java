package ru.pyshinskiy.tm.api.wbs;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.AbstractWBS;

import java.util.List;

public interface IAbstractWBSRepository<T extends AbstractWBS> {

    @NotNull
    List<T> findAll() throws Exception;

    @Nullable
    T findOne(@NotNull final String id) throws Exception;

    @Nullable
    T persist(@NotNull final T t) throws Exception;

    @NotNull
    List<T> persist(@NotNull final List<T> ts) throws Exception;

    @Nullable
    T merge(@NotNull final T t) throws Exception;

    void remove(@NotNull final String id) throws Exception;

    void removeAll() throws Exception;

    @NotNull
    List<T> findAll(@NotNull final String userId) throws Exception;

    @Nullable
    T findOne(@NotNull final String userId, @NotNull final String id) throws Exception;

    @Nullable
    List<T> findByName(@NotNull final String userId, @NotNull final String name) throws Exception;

    @Nullable
    List<T> findByDescription(@NotNull final String userId, @NotNull final String description) throws Exception;

    void remove(@NotNull final String userId, @NotNull final String id) throws Exception;

    void removeAll(@NotNull final String userId) throws Exception;

    @NotNull
    List<T> sortByCreateTime(@NotNull final String userId, final int direction) throws Exception;

    @NotNull
    List<T> sortByStartDate(@NotNull final String userId, final int direction) throws Exception;

    @NotNull
    List<T> sortByFinishDate(@NotNull final String userId, final int direction) throws Exception;

    @NotNull
    List<T> sortByStatus(@NotNull final String userId, final int direction) throws Exception;
}

package ru.pyshinskiy.tm.api.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.wbs.IAbstractVBSService;
import ru.pyshinskiy.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IAbstractVBSService<Task> {

    @NotNull
    List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception;

    void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception;

    void removeAttachedTasks() throws Exception;

    void removeAttachedTasks(@NotNull final String userId) throws Exception;
}

package ru.pyshinskiy.tm.api.session;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.repository.IRepository;
import ru.pyshinskiy.tm.entity.Session;

public interface ISessionRepository extends IRepository<Session> {

    @Nullable
    Session findOne(@NotNull final String userId, @NotNull final String id) throws Exception;

    void remove(@NotNull final String userId, @NotNull final String id) throws Exception;
}

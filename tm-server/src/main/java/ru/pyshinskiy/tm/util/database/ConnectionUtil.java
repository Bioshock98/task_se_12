package ru.pyshinskiy.tm.util.database;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import static ru.pyshinskiy.tm.constant.AppConst.RESOURCE_DIR;

public final class ConnectionUtil {

    @NotNull
    public static Connection getConnection() throws  Exception {
        @NotNull final Properties properties = new Properties();
        @NotNull final InputStream propFile = new FileInputStream( RESOURCE_DIR + File.separator + "database.properties");
        properties.load(propFile);
        Class.forName(properties.getProperty("jdbcDriver"));
        return DriverManager.getConnection(properties.getProperty("db.host"), properties.getProperty("db.login"),
                properties.getProperty("db.password"));
    }
}
